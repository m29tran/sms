package com.messaging.sms;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by michaeltran on 2015-01-02.
 */
public class ThreadActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final Uri INBOX_URI = Uri.parse("content://sms/");
    private List<SmsModel> mSmsModelList;
    private ThreadArrayAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_thread);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSmsModelList = new LinkedList<SmsModel>();
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.messenger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        CursorLoader cursorLoader = new CursorLoader(this,
                INBOX_URI,
                new String[]{"DISTINCT person", "address", "date", "body", "thread_id"},
                "thread_id IS NOT NULL) GROUP BY (thread_id",
                null,
                null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int person = cursor.getColumnIndex(cursor.getColumnNames()[0]);
        int address = cursor.getColumnIndex(cursor.getColumnNames()[1]);
        int date = cursor.getColumnIndex(cursor.getColumnNames()[2]);
        int message = cursor.getColumnIndex(cursor.getColumnNames()[3]);
        int thread = cursor.getColumnIndex(cursor.getColumnNames()[4]);

        if (cursor.moveToFirst()) {
            do {
                mSmsModelList.add(
                        new SmsModel(
                                cursor.getString(person),
                                cursor.getString(address),
                                cursor.getLong(date),
                                cursor.getString(message),
                                0,
                                cursor.getLong(thread))
                );
            } while (cursor.moveToNext());
        }
        mAdapter = new ThreadArrayAdapter(this, R.layout.thread_list_item, mSmsModelList);
        getListView().setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        Intent intent = new Intent();
        intent.setClass(this, MessengerActivity.class);
        intent.putExtra("thread_id", mSmsModelList.get(position).getThreadId());
        startActivity(intent);
    }
}
