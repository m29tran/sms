package com.messaging.sms;

import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Telephony;
import android.view.Menu;
import android.view.MenuItem;
import java.util.LinkedList;
import java.util.List;


public class MessengerActivity extends ListActivity implements LoaderManager.LoaderCallbacks<Cursor> {

    public static final Uri INBOX_URI = Uri.parse("content://sms/");
    private List<SmsModel> mSmsModelList;
    private SmsArrayAdapter mAdapter;
    private Long mThreadId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messenger);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSmsModelList = new LinkedList<SmsModel>();

        mThreadId = getIntent().getLongExtra("thread_id", 0);
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.messenger, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Loader onCreateLoader(int i, Bundle bundle) {
        String selection = "thread_id = ?";
        String[] selectionArgs = {mThreadId.toString()};
        CursorLoader cursorLoader = new CursorLoader(this, INBOX_URI, null, selection, selectionArgs, null);
        return cursorLoader;
    }

    @Override
    public void onLoadFinished(Loader<Cursor> cursorLoader, Cursor cursor) {
        int person = cursor.getColumnIndex(Telephony.Sms.PERSON);
        int address = cursor.getColumnIndex(Telephony.Sms.ADDRESS);
        int date = cursor.getColumnIndex(Telephony.Sms.DATE);
        int message = cursor.getColumnIndex(Telephony.Sms.BODY);
        int type = cursor.getColumnIndex(Telephony.Sms.TYPE);
        int thread = cursor.getColumnIndex(Telephony.Sms.THREAD_ID);

        if (cursor.moveToFirst()) {
            do {
                mSmsModelList.add(
                        new SmsModel(
                                cursor.getString(person),
                                cursor.getString(address),
                                cursor.getLong(date),
                                cursor.getString(message),
                                cursor.getInt(type),
                                cursor.getLong(thread))
                );
            } while (cursor.moveToNext());
        }
        mAdapter = new SmsArrayAdapter(this, R.layout.messenger_list_item, mSmsModelList);
        getListView().setAdapter(mAdapter);
    }

    @Override
    public void onLoaderReset(Loader loader) {
    }
}
