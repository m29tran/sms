package com.messaging.sms;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by michaeltran on 2014-12-11.
 */
public class SmsModel {
    private String person;
    private String address;
    private Long date;
    private String message;
    private int type;
    private long threadId;

    public SmsModel(String person, String address, long date, String message, int type, long threadId) {
        this.address = address;
        this.date = date;
        this.message = message;
        this.person = person;
        this.type = type;
        this.threadId= threadId;
    }

    public String getPerson() {
        return person;
    }

    public String getAddress() {
        return address;
    }

    public Long getEpochDate() {
        return date;
    }

    public Date getDate() {
        return new Date(date);
    }

    public String getTimeString() {
        long millis = System.currentTimeMillis() - date;
        long seconds = millis / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        String value;
        if (minutes < 2) {
            value = "A moment ago";
        } else if (minutes < 60) {
            value = (minutes - (hours*60)) + " minutes ago";
        } else if (minutes < 120) {
            value = "Over an hour ago";
        } else if (hours < 24) {
            value = (hours - (days*24)) + " hours ago";
        } else if (days < 2) {
            value = "yesterday";
        } else if (days < 7) {
            value = new SimpleDateFormat("EEE d").format(getDate());
        } else if (days < 365) {
            value = new SimpleDateFormat("MMM d").format(getDate());
        } else {
            value = new SimpleDateFormat("MM/dd/yyyy").format(getDate());
        }
        return value + ", " + (String) new SimpleDateFormat("h:mm a").format(getDate());
    }

    public int getType() {
        return type;
    }

    public String getMessage() {
        return message;
    }

    public Long getThreadId() {
        return threadId;
    }
}