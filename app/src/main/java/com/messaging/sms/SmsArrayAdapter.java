package com.messaging.sms;

import android.app.Activity;
import android.content.Context;
import android.provider.Telephony;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by michaeltran on 2014-12-11.
 */
public class SmsArrayAdapter extends ArrayAdapter<SmsModel> {

    Context mContext;
    int mLayoutResourceId;
    List<SmsModel> mData;

    public SmsArrayAdapter(Context context, int layoutResourceId,
                           List<SmsModel> data) {
        super(context, layoutResourceId, data);
        mData = data;
        mContext = context;
        mLayoutResourceId = layoutResourceId;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        SmsHolder holder = null;
        if(row == null)
        {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(mLayoutResourceId, parent, false);

            holder = new SmsHolder();
            holder.txtAddress = (TextView)row.findViewById(R.id.address);
            holder.txtMessage = (TextView)row.findViewById(R.id.message);
            holder.txtDate = (TextView)row.findViewById(R.id.date);
            holder.imgUser = (ImageView)row.findViewById(R.id.user_thumbnail);
            holder.imgContact = (ImageView)row.findViewById(R.id.contact_thumbnail);

            row.setTag(holder);
        }
        else
        {
            holder = (SmsHolder) row.getTag();
        }

        SmsModel sms = mData.get(position);
        holder.txtAddress.setText(sms.getAddress());
        holder.txtMessage.setText(sms.getMessage());
        holder.txtDate.setText(sms.getTimeString());

        if (sms.getType() == Telephony.Sms.MESSAGE_TYPE_SENT) {
            holder.imgContact.setVisibility(View.GONE);
            holder.imgUser.setVisibility(View.VISIBLE);
            holder.txtDate.setGravity(Gravity.RIGHT);
            holder.txtMessage.setGravity(Gravity.RIGHT);
            holder.txtAddress.setGravity(Gravity.RIGHT);
        } else {
            holder.imgContact.setVisibility(View.VISIBLE);
            holder.imgUser.setVisibility(View.GONE);
            holder.txtDate.setGravity(Gravity.LEFT);
            holder.txtMessage.setGravity(Gravity.LEFT);
            holder.txtAddress.setGravity(Gravity.LEFT);
        }

        return row;

    }

    class SmsHolder {
        //TextView txtName;
        TextView txtAddress;
        TextView txtDate;
        TextView txtMessage;
        ImageView imgContact;
        ImageView imgUser;
    }
}